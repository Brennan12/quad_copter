#include <stdint.h>
#include <stdbool.h>
#include "ble.h"
#include "ble_srv_common.h"

#define drone_SERVICE_UUID_BASE         {0xBC, 0x8A, 0xBF, 0x45, 0xCA, 0x05, 0x50, 0xBA, \
                                          0x40, 0x42, 0xB0, 0x00, 0xC9, 0xAD, 0x64, 0xF3}

#define drone_SERVICE_UUID               0x1400
#define drone_VALUE_CHAR_UUID            0x1401

#define BLE_drone_DEF(_name)                                                                          \
static ble_drone_t _name;                                                                             \
NRF_SDH_BLE_OBSERVER(_name ## _obs,                                                                 \
                     BLE_HRS_BLE_OBSERVER_PRIO,                                                     \
                     ble_drone_on_ble_evt, &_name)
typedef struct ble_drone_s ble_drone_t;

typedef enum
{
    BLE_DRONE_EVT_NOTIFICATION_ENABLED,                             /**< drone value notification enabled event. */
    BLE_DRONE_EVT_NOTIFICATION_DISABLED,                            /**< drone value notification disabled event. */
    BLE_DRONE_EVT_DISCONNECTED,
    BLE_DRONE_EVT_CONNECTED
} ble_drone_evt_type_t;
typedef struct
{
    ble_drone_evt_type_t evt_type;                                  /**< Type of event. */
} ble_drone_evt_t;

typedef void (*ble_drone_evt_handler_t) (ble_drone_t * p_drone, ble_drone_evt_t * p_evt);

typedef struct
{
    ble_drone_evt_handler_t         evt_handler;                    /**< Event handler to be called for handling events in the drone Service. */
    uint8_t                       initial_drone_value;           /**< Initial drone value */
    ble_srv_cccd_security_mode_t  drone_value_char_attr_md;     /**< Initial security level for drone characteristics attribute */
} ble_drone_init_t;

/**@brief drone Service structure. This contains various status information for the service. */
struct ble_drone_s
{
    ble_drone_evt_handler_t         evt_handler;                    /**< Event handler to be called for handling events in the drone Service. */
    uint16_t                      service_handle;                 /**< Handle of drone Service (as provided by the BLE stack). */
    ble_gatts_char_handles_t      drone_value_handles;           /**< Handles related to the drone Value characteristic. */
    uint16_t                      conn_handle;                    /**< Handle of the current connection (as provided by the BLE stack, is BLE_CONN_HANDLE_INVALID if not in a connection). */
    uint8_t                       uuid_type; 
};

uint32_t ble_drone_init(ble_drone_t * p_drone, const ble_drone_init_t * p_drone_init);
static uint32_t drone_value_char_add(ble_drone_t * p_drone, const ble_drone_init_t * p_drone_init);
void ble_drone_on_ble_evt( ble_evt_t const * p_ble_evt, void * p_context);
static void on_connect(ble_drone_t * p_drone, ble_evt_t const * p_ble_evt);
static void on_disconnect(ble_drone_t * p_drone, ble_evt_t const * p_ble_evt);
static void on_write(ble_drone_t * p_drone, ble_evt_t const * p_ble_evt);
uint32_t ble_drone_drone_value_update(ble_drone_t * p_drone, uint8_t drone_value);